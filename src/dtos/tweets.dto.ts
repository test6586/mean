import { IsString } from 'class-validator';

export class CreateTweetDto {
  @IsString()
  public author_id: string;

  @IsString()
  public created_at: string;

  @IsString()
  public id: string;

  @IsString()
  public text: string;
}
