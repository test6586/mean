import { Router } from 'express';
import TweetsController from '@controllers/tweet.controller';
import { CreateTweetDto } from '@dtos/tweets.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';

class TweetsRoute implements Routes {
  public path = '/tweets';
  public router = Router();
  public tweetsController = new TweetsController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.tweetsController.getTweets);
    this.router.get(`${this.path}/:id(\\d+)`, this.tweetsController.getTweetById);
    this.router.post(`${this.path}`, this.tweetsController.createTweet);
    this.router.put(`${this.path}/:id(\\d+)`, validationMiddleware(CreateTweetDto, 'body', true), this.tweetsController.updateTweet);
    this.router.delete(`${this.path}/:id(\\d+)`, this.tweetsController.deleteTweet);
  }
}

export default TweetsRoute;
