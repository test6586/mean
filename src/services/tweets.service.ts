import { CreateTweetDto } from '@dtos/tweets.dto';
import { HttpException } from '@exceptions/HttpException';
import { Tweet } from '@interfaces/tweet.interface';
import tweetModel from '@examples/tweets.model';
import { isEmpty } from '@utils/util';
import { logger } from '@/utils/logger';

class TweetService {
  public tweets = tweetModel;

  public async findAllTweet(): Promise<Tweet[]> {
    const tweets: Tweet[] = this.tweets;
    return tweets;
  }

  public async findTweetById(tweetId: string): Promise<Tweet> {
    const findTweet: Tweet = this.tweets.find(tweet => tweet.id === tweetId);
    if (!findTweet) throw new HttpException(409, "You're not tweet");

    return findTweet;
  }

  public createTweet(tweetData) {
    logger.info(tweetData);
    const createTweetData: Tweet = { ...tweetData.data };
    this.tweets = [...this.tweets, createTweetData];
  }

  public async updateTweet(tweetId: string, tweetData: CreateTweetDto): Promise<Tweet[]> {
    if (isEmpty(tweetData)) throw new HttpException(400, "You're not tweetData");

    const findTweet: Tweet = this.tweets.find(tweet => tweet.id == tweetId);
    if (!findTweet) throw new HttpException(409, "You're not tweet");

    const updateTweetData: Tweet[] = this.tweets.map((tweet: Tweet) => {
      if (tweet.id === findTweet.id) tweet = { id: tweetId, ...tweetData };
      return tweet;
    });

    return updateTweetData;
  }

  public async deleteTweet(tweetId: string): Promise<Tweet[]> {
    const findTweet: Tweet = this.tweets.find(tweet => tweet.id === tweetId);
    if (!findTweet) throw new HttpException(409, "You're not tweet");

    const deleteTweetData: Tweet[] = this.tweets.filter(tweet => tweet.id !== findTweet.id);
    return deleteTweetData;
  }
}

export default TweetService;
