export interface Tweet {
  author_id: string;
  created_at: string;
  id: string;
  text: string;
}
