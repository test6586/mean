import { NextFunction, Request, Response } from 'express';
import { CreateTweetDto } from '@dtos/tweets.dto';
import { Tweet } from '@interfaces/tweet.interface';
import tweetService from '@services/tweets.service';
import needle from 'needle';

class TweetsController {
  public tweetService = new tweetService();

  public getTweets = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllTweetsData: Tweet[] = await this.tweetService.findAllTweet();

      res.status(200).json({ data: findAllTweetsData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getTweetById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const tweetId = String(req.params.id);
      const findOneTweetData: Tweet = await this.tweetService.findTweetById(tweetId);

      res.status(200).json({ data: findOneTweetData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public createTweet = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      // The code below sets the bearer token from your environment variables
      // To set environment variables on macOS or Linux, run the export command below from the terminal:
      // export BEARER_TOKEN='YOUR-TOKEN'
      const token = 'AAAAAAAAAAAAAAAAAAAAAFoeXQEAAAAAJcVSE9dmAyHwuVmd87ocLNpCqJA%3DmspVNfoJjQMHuTytmRNvt5quYOWp0XID7cJJwMxRiDnW3zrgd4';

      const rulesURL = 'https://api.twitter.com/2/tweets/search/stream/rules';
      const streamURL = 'https://api.twitter.com/2/tweets/search/stream?tweet.fields=created_at,text,author_id&user.fields=created_at,name,username';

      // this sets up two rules - the value is the search terms to match on, and the tag is an identifier that
      // will be applied to the Tweets return to show which rule they matched
      // with a standard project with Basic Access, you can add up to 25 concurrent rules to your stream, and
      // each rule can be up to 512 characters long

      // Edit rules as desired below
      const rules = [
        {
          value: 'dog has:images -is:retweet',
          tag: 'dog pictures',
        },
        {
          value: 'cat has:images -grumpy',
          tag: 'cat pictures',
        },
      ];

      async function getAllRules() {
        const response = await needle('get', rulesURL, {
          headers: {
            authorization: `Bearer ${token}`,
          },
        });

        if (response.statusCode !== 200) {
          console.log(`Error: ${response.statusMessage} , ${response.statusCode}`);
          throw new Error(response.body);
        }

        return response.body;
      }

      async function deleteAllRules(rules) {
        if (!Array.isArray(rules.data)) {
          return null;
        }

        const ids = rules.data.map(rule => rule.id);

        const data = {
          delete: {
            ids: ids,
          },
        };

        const response = await needle('post', rulesURL, data, {
          headers: {
            'content-type': 'application/json',
            authorization: `Bearer ${token}`,
          },
        });

        if (response.statusCode !== 200) {
          throw new Error(response.body);
        }

        return response.body;
      }

      async function setRules() {
        const data = {
          add: rules,
        };

        const response = await needle('post', rulesURL, data, {
          headers: {
            'content-type': 'application/json',
            authorization: `Bearer ${token}`,
          },
        });

        if (response.statusCode !== 201) {
          throw new Error(response.body);
        }

        return response.body;
      }

      function streamConnect(retryAttempt) {
        const stream = needle.get(streamURL, {
          headers: {
            'User-Agent': 'v2FilterStreamJS',
            Authorization: `Bearer ${token}`,
          },
          timeout: 20000,
        });

        stream
          .on('data', data => {
            try {
              const json = JSON.parse(data);
              console.log(json);
              this.tweetService.createTweet(json);
              // A successful connection resets retry count.
              retryAttempt = 0;
            } catch (e) {
              if (data.detail === 'This stream is currently at the maximum allowed connection limit.') {
                console.log(data.detail);
                process.exit(1);
              } else {
                // Keep alive signal received. Do nothing.
              }
            }
          })
          .on('err', error => {
            if (error.code !== 'ECONNRESET') {
              console.log(error.code);
              process.exit(1);
            } else {
              // This reconnection logic will attempt to reconnect when a disconnection is detected.
              // To avoid rate limits, this logic implements exponential backoff, so the wait time
              // will increase if the client cannot reconnect to the stream.
              setTimeout(() => {
                console.warn('A connection error occurred. Reconnecting...');
                streamConnect(++retryAttempt);
              }, 2 ** retryAttempt);
            }
          });

        return stream;
      }

      (async () => {
        let currentRules;

        try {
          // Gets the complete list of rules currently applied to the stream
          currentRules = await getAllRules();
          // // Delete all rules. Comment the line below if you want to keep your existing rules.
          await deleteAllRules(currentRules);
          // // Add rules to the stream. Comment the line below if you don't want to add new rules.
          await setRules();
        } catch (e) {
          console.error(e);
          process.exit(1);
        }
        // Listen to the stream.
        streamConnect(0);
      })();
    } catch (error) {
      next(error);
    }
    res.status(201).json({ data: {}, message: 'Adding twits from twitter stream' });
  };

  public updateTweet = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const tweetId = String(req.params.id);
      const tweetData: CreateTweetDto = req.body;
      const updateTweetData: Tweet[] = await this.tweetService.updateTweet(tweetId, tweetData);

      res.status(200).json({ data: updateTweetData, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteTweet = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const tweetId = String(req.params.id);
      const deleteTweetData: Tweet[] = await this.tweetService.deleteTweet(tweetId);

      res.status(200).json({ data: deleteTweetData, message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}

export default TweetsController;
