import { Tweet } from '@interfaces/tweet.interface';

const tweetModel: Tweet[] = [
  {
    author_id: '1193165949836259328',
    created_at: '2021-12-22T13:55:44.000Z',
    id: '1473653483517530115',
    text: "RT @HRJFEED: wherever there's a cat, there's a renjun nearby https://t.co/h2PdGpIxGd",
  },
];

export default tweetModel;
