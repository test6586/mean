## Pasos seguidos para el desarrollo del 'Producto minimo viable' de forma mas rápida

1. Se ha usado la libreria de <https://www.npmjs.com/package/typescript-express-starter> para montar el proyecto base.
2. Se ha usado el ejemplo básico de twitter developer para el streaming de twits con los 'rules' por defecto.
3. Se ha usado <https://developer.twitter.com/apitools/api> para generar los endpoint del streaming.

## Base de datos

Respecto a la base de datos hubiera usado mongodb con mongoose como framework pero para el ejemplo lo guardo en memoria.

## Cosas a mejorar

1. Guardar datos en mongodb usando framework mongoose y como libreria extra 'mongoose-paginate-v2' para poder hacer uso de la paginación y el orden de los elementos.

2. Añadir test unitarios para el modelo de tweets.

3. Añadir filtros por fechas y parametros de usuario.

4. Extraer el streaming de twitter a un middleware externos y no hacerlo desde el controlador.
